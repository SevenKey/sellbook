package vo;

/**
 * Web APP 统一返回结果
 * Created by weijianyu on 2016/12/16.
 */
public class ResultVO<T> {

    private int code;

    private T data;

    private String errorMessage;

    public int getCode() {
        return code;
    }

    public T getData() {
        return data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "ResultVO{" +
                "code=" + code +
                ", data=" + data +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
