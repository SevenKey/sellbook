package service;

import dao.SlideMapper;
import entity.Slide;
import entity.SlideExample;
import httpcode.HttpStatusCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import vo.ResultVO;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Properties;

/**
 * 轮播图服务
 * Created by weijianyu on 2017/5/9.
 */
@Service
public class SlideService {

    private static final Logger logger = LogManager.getLogger(SlideService.class);

    private final SlideMapper slideMapper;

    /**
     * 构造方法
     *
     * @param slideMapper slide dao层服务
     */
    @Autowired
    public SlideService(SlideMapper slideMapper) {
        Assert.notNull(slideMapper, "slideMapper must not null!");
        this.slideMapper = slideMapper;
    }

    /**
     * 获取轮播图
     *
     * @return 轮播图信息
     */
    public ResultVO<List<Slide>> list() {
        ResultVO<List<Slide>> result = new ResultVO<List<Slide>>();

        SlideExample slideExample = new SlideExample();
        slideExample.or().getAllCriteria();
        List<Slide> slides = slideMapper.selectByExample(slideExample);

        if (slides == null || slides.isEmpty()) {
            logger.error("轮播图信息为空");
            result.setCode(HttpStatusCodeEnum.ERROR.getCode());
            result.setData(null);
            result.setErrorMessage("轮播图为空");
        } else {
            result.setCode(HttpStatusCodeEnum.SUCCESS.getCode());
            result.setData(slides);
            result.setErrorMessage("");
        }

        return result;
    }

}
