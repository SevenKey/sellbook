package service;

import com.google.common.collect.Lists;
import dao.BookMapper;
import entity.Book;
import entity.BookExample;
import enums.BookStatusEnum;
import enums.QueryEnum;
import httpcode.HttpStatusCodeEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import vo.ResultVO;

import java.util.List;
import java.util.Random;

/**
 * 书籍服务
 * Created by weijianyu on 2017/4/24.
 */
@Service
public class BookService {

    private static final Logger logger = LogManager.getLogger(BookService.class);

    private final BookMapper bookMapper;
    private final CategoryService categoryService;

    /**
     * 构造函数
     * 初始化
     *
     * @param bookMapper book书籍服务
     */
    @Autowired
    public BookService(BookMapper bookMapper, CategoryService categoryService) {
        Assert.notNull(bookMapper, "bookMapper must not null");
        this.bookMapper = bookMapper;
        Assert.notNull(categoryService, "categoryService must not null");
        this.categoryService = categoryService;
    }

    /**
     * 新书推荐
     *
     * @return 新书列表
     */
    public ResultVO<List<Book>> newBooksList() {
        ResultVO<List<Book>> result = new ResultVO<List<Book>>();
        BookExample bookExample = new BookExample();
        bookExample.or().andTypeEqualTo((short) BookStatusEnum.NEWS.getStatus());
        List<Book> newBooks = bookMapper.selectByExample(bookExample);
        if (newBooks.isEmpty()) {
            logger.warn("新书推荐为空");
        }

        result.setCode(HttpStatusCodeEnum.SUCCESS.getCode());
        result.setData(newBooks);
        result.setErrorMessage("");
        return result;
    }

    /**
     * 查询id书籍
     *
     * @param id 书籍id
     * @return 该id书籍的详细信息
     */
    public ResultVO<Book> bookDetail(int id) {
        ResultVO<Book> result = new ResultVO<Book>();
        Book book = bookMapper.selectByPrimaryKey(id);
        if (book == null) {
            logger.info("未查到id:{}的书籍信息", id);
            result.setCode(HttpStatusCodeEnum.ERROR.getCode());
            result.setData(null);
            result.setErrorMessage("未查到书籍信息");
            return result;
        }
        result.setCode(HttpStatusCodeEnum.SUCCESS.getCode());
        result.setData(book);
        result.setErrorMessage("");
        return result;
    }

    /**
     * 排行榜
     *
     * @return 返回数据库中排行榜的信息
     */
    public ResultVO<List<Book>> rankBooks() {
        ResultVO<List<Book>> result = new ResultVO<List<Book>>();
        BookExample bookExample = new BookExample();
        bookExample.or().andTypeEqualTo((short) BookStatusEnum.RANK.getStatus());
        List<Book> rankBooks = bookMapper.selectByExample(bookExample);
        if (rankBooks.isEmpty()) {
            logger.warn("排行榜为空");
        }

        result.setCode(HttpStatusCodeEnum.SUCCESS.getCode());
        result.setData(rankBooks);
        result.setErrorMessage("");
        return result;
    }

    /**
     * 搜索书籍
     *
     * @param bookName 书名
     * @return 如果不存在返回推荐书籍，否则返回查找到的书籍
     */
    public ResultVO<List<Book>> search(String bookName) {
        ResultVO<List<Book>> result = new ResultVO<List<Book>>();

        List<Book> books = bookMapper.selectByBookName(bookName);
        if (books.isEmpty()) {
            logger.info("搜索结果为空");
            BookExample bookExample = new BookExample();
            bookExample.or().andTypeEqualTo((short) BookStatusEnum.RECOMMEND.getStatus());
            List<Book> recommends = bookMapper.selectByExample(bookExample);
            if (recommends.isEmpty()) {
                logger.error("推荐书籍未设置!");
                result.setCode(QueryEnum.ERROR.getCode());
                result.setData(null);
                result.setErrorMessage("推荐书籍未设置!");
                return result;
            }
            result.setCode(QueryEnum.EMPTY.getCode());
            result.setData(recommends);
            result.setErrorMessage("");
            return result;
        }
        result.setCode(QueryEnum.SUCCESS.getCode());
        result.setData(books);
        result.setErrorMessage("");
        return result;
    }

    /**
     * 查询分类书籍
     *
     * @param id 父类id
     * @return 子类书籍
     */
    public ResultVO<List<Book>> classify(int id) {
        ResultVO<List<Book>> result = new ResultVO<List<Book>>();

        BookExample bookExample = new BookExample();
        bookExample.or().andParentEqualTo((short) id);
        List<Book> books = bookMapper.selectByExample(bookExample);
        if (books.isEmpty()) {
            logger.info("书籍服务 书籍表中父id：{} 结果集为空。查询是否是一级目录", id);
            List<Short> ids = categoryService.son(id);
            if (ids.isEmpty()) {
                logger.error("书籍服务 不是一级目录");
                result.setCode(HttpStatusCodeEnum.ERROR.getCode());
                result.setErrorMessage("没有找到子类信息");
                return result;
            } else {
                logger.error("书籍服务 是一级目录继续查询");
                bookExample.clear();
                bookExample.or().andParentIn(ids);
                List<Book> datas = bookMapper.selectByExample(bookExample);
                if (datas.isEmpty()) {
                    logger.info("书籍服务 ids：{}书籍信息为空", ids);
                }
                result.setCode(HttpStatusCodeEnum.SUCCESS.getCode());
                result.setData(datas);
                result.setErrorMessage("");
                return result;
            }
        } else {
            result.setCode(HttpStatusCodeEnum.SUCCESS.getCode());
            result.setData(books);
            result.setErrorMessage("");
            return result;
        }
    }

    public static void main(String[] args) {
        double a = 0.1;
        double b = 0.2;
        System.out.println(a+b);

        float a1 = 0.1f;
        float b1 = 0.2f;
        System.out.println(a1+b1);
    }

}
