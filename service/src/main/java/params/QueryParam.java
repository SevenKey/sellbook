package params;

/**
 * 查询参数
 * Created by weijianyu on 2017/5/18.
 */
public class QueryParam {
    private String bookName;

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}
