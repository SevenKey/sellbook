package enums;

/**
 * 注册枚举
 * Created by weijianyu on 2017/4/25.
 */
public enum RegisterEnum {

    SUCCESS(210, "注册成功"),

    ERROR(510, "注册失败"),

    VAIL_USERNAME(511, "请输入用户名"),
    VAIL_EMAIL(512, "请输入邮箱"),
    VAIL_PASSWORD(513, "请输入密码"),
    VAIL_PHONE(514, "请输入电话"),

    ERROR_USERNAME(515, "用户名已存在"),
    ERROR_EMAIL(516, "邮箱已被注册"),
    ERROR_PHONE(517, "电话已被注册"),

    ERROR_FORMAT_PASSWORD(518, "密码格式错误");

    private int code;
    private String message;

    private RegisterEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
