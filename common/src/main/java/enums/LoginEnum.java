package enums;

/**
 * 登录枚举
 * Created by weijianyu on 2017/4/25.
 */
public enum LoginEnum {
    SUCCESS(220, "登录成功"),

    ERROR(520, "email或者密码错误"),

    VAIL_EMAIL(521, "请输入邮箱"),
    VAIL_PASSWORD(522, "请输入密码"),

    ERROR_FORMAT_PASSWORD(523, "密码格式错误"),
    ERROR_LOGIN(524, "登录失败,请联系后台管理"),

    ERROR_COOKIE_IMITATION(525, "cookie被篡改"),
    ERROR_COOKIE_TIMEOUT(526, "cookie过期,请重新登录"),
    ERROR_COOKIE_NULL(527, "请重新登录"),
    ERROR_COOKIE_FAILURE(528, "cookie无效请重新登录");

    private int code;
    private String message;

    private LoginEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
