package enums;

/**
 * 查询结果枚举类
 * Created by weijianyu on 2017/5/18.
 */
public enum QueryEnum {
    SUCCESS(230, "查询成功"),
    EMPTY(231, "查询结果为空"),

    ERROR(530, "查询失败");

    private int code;
    private String message;

    QueryEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
