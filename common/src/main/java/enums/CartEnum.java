package enums;

/**
 * 购物车枚举
 * Created by weijianyu on 2017/5/19.
 */
public enum CartEnum {

    SUCCESS(240, "成功"),
    ADD_SUCCESS(241, "添加成功"),

    ERROR(540, "失败"),
    VAIL_USER(541, "用户为空"),
    VAIL_BOOK(542, "书籍为空"),
    VAIL_AMOUNT(543, "数量为空"),

    CART_USER(545, "请先登录"),
    DELETE_ID(546, "id为空");


    private int code;
    private String message;

    CartEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
