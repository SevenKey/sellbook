package enums;

/**
 * 书 状态
 * Created by weijianyu on 2017/5/12.
 */
public enum BookStatusEnum {

    NEWS(1, "新书推荐"),
    RANK(2, "排行榜"),
    RECOMMEND(3, "推荐");
    private int status;
    private String message;

    BookStatusEnum(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
