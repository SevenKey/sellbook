package entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.MoreObjects;

/**
 * @author weijianyu
 */
public class RowItem {
    @JSONField(name = "mt_appkey")
    private String mtAppkey;
    @JSONField(name = "_source")
    private String source;
    @JSONField(name = "mt_level")
    private String mtLevel;
    @JSONField(name = "mt_datetime")
    private String mtDatetime;
    @JSONField(name = "mt_logger_name")
    private String mtLoggerName;
    @JSONField(name = "es_timestamp")
    private String esTimestamp;
    @JSONField(name = "message")
    private String message;
    @JSONField(name = "mt_thread")
    private String mtThread;
    @JSONField(name = "mt_servername")
    private String mtServername;

    public String getMtAppkey() {
        return mtAppkey;
    }

    public void setMtAppkey(String mtAppkey) {
        this.mtAppkey = mtAppkey;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getMtLevel() {
        return mtLevel;
    }

    public void setMtLevel(String mtLevel) {
        this.mtLevel = mtLevel;
    }

    public String getMtDatetime() {
        return mtDatetime;
    }

    public void setMtDatetime(String mtDatetime) {
        this.mtDatetime = mtDatetime;
    }

    public String getMtLoggerName() {
        return mtLoggerName;
    }

    public void setMtLoggerName(String mtLoggerName) {
        this.mtLoggerName = mtLoggerName;
    }

    public String getEsTimestamp() {
        return esTimestamp;
    }

    public void setEsTimestamp(String esTimestamp) {
        this.esTimestamp = esTimestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMtThread() {
        return mtThread;
    }

    public void setMtThread(String mtThread) {
        this.mtThread = mtThread;
    }

    public String getMtServername() {
        return mtServername;
    }

    public void setMtServername(String mtServername) {
        this.mtServername = mtServername;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("mtAppkey", mtAppkey)
                .add("source", source)
                .add("mtLevel", mtLevel)
                .add("mtDatetime", mtDatetime)
                .add("mtLoggerName", mtLoggerName)
                .add("esTimestamp", esTimestamp)
                .add("message", message)
                .add("mtThread", mtThread)
                .add("mtServername", mtServername)
                .toString();
    }
}
