package entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.MoreObjects;

/**
 * @author weijianyu
 */
public class LogCenterResponse {
    @JSONField(name = "data")
    private LogCenterModel data;
    @JSONField(name = "error_code")
    private String errorCode;
    @JSONField(name = "error_msg")
    private String errorMsg;

    public LogCenterModel getData() {
        return data;
    }

    public void setData(LogCenterModel data) {
        this.data = data;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("data", data)
                .add("errorCode", errorCode)
                .add("errorMsg", errorMsg)
                .toString();
    }
}
