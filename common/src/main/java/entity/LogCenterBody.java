package entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author weijianyu
 */
public class LogCenterBody {
    @JSONField(name = "index_name")
    private String indexName = "log.com.sankuai.train.train.agentwin_all";
    private boolean cluster = false;
    private Query query;
    private Sort sort;
    private int timeout = 30000;
    private int from = 0;
    private int size = 1000;

    public static LogCenterBody builtDefaultBody() {
        LogCenterBody body = new LogCenterBody();
        // query
        Query query = new Query();
        Bool bool = new Bool();

        List<Filter> filters = Lists.newArrayList();
        Filter filter = new Filter();
        Range range = new Range();
        EsTimestamp esTimestamp = new EsTimestamp();
        range.setEsTimestamp(esTimestamp);
        filter.setRange(range);
        filters.add(filter);
        bool.setFilters(filters);

        List<Must> musts = Lists.newArrayList();
        Must must = new Must();
        QueryString queryString = new QueryString();
        must.setQueryString(queryString);
        musts.add(must);
        bool.setMusts(musts);
        query.setBool(bool);

        Sort sort = new Sort();
        body.setQuery(query);
        body.setSort(sort);
        return body;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public boolean isCluster() {
        return cluster;
    }

    public void setCluster(boolean cluster) {
        this.cluster = cluster;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("indexName", indexName)
                .add("cluster", cluster)
                .add("query", query)
                .add("sort", sort)
                .add("timeout", timeout)
                .add("from", from)
                .add("size", size)
                .toString();
    }

    public static class Query {
        private Bool bool;

        public Bool getBool() {
            return bool;
        }

        public void setBool(Bool bool) {
            this.bool = bool;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("bool", bool)
                    .toString();
        }
    }

    public static class Bool {
        @JSONField(name = "filter")
        private List<Filter> filters;
        @JSONField(name = "must")
        private List<Must> musts;

        public List<Filter> getFilters() {
            return filters;
        }

        public void setFilters(List<Filter> filters) {
            this.filters = filters;
        }

        public List<Must> getMusts() {
            return musts;
        }

        public void setMusts(List<Must> musts) {
            this.musts = musts;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("filters", filters)
                    .add("musts", musts)
                    .toString();
        }
    }

    public static class Filter {
        private Range range;

        public Range getRange() {
            return range;
        }

        public void setRange(Range range) {
            this.range = range;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("range", range)
                    .toString();
        }
    }

    public static class Must {
        @JSONField(name = "query_string")
        private QueryString queryString;

        public QueryString getQueryString() {
            return queryString;
        }

        public void setQueryString(QueryString queryString) {
            this.queryString = queryString;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("queryString", queryString)
                    .toString();
        }
    }

    public static class QueryString {
        private String query = "message:\"验证码机器学习样本\"";
        @JSONField(name = "lowercase_expanded_terms")
        private boolean lowercaseExpandedTerms = false;

        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }

        public boolean isLowercaseExpandedTerms() {
            return lowercaseExpandedTerms;
        }

        public void setLowercaseExpandedTerms(boolean lowercaseExpandedTerms) {
            this.lowercaseExpandedTerms = lowercaseExpandedTerms;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("query", query)
                    .add("lowercaseExpandedTerms", lowercaseExpandedTerms)
                    .toString();
        }
    }

    public static class Range {
        @JSONField(name = "es_timestamp")
        private EsTimestamp esTimestamp;

        public EsTimestamp getEsTimestamp() {
            return esTimestamp;
        }

        public void setEsTimestamp(EsTimestamp esTimestamp) {
            this.esTimestamp = esTimestamp;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("esTimestamp", esTimestamp)
                    .toString();
        }
    }


    public static class EsTimestamp {
        private String gte = "2018/04/17 03:55:22 +0800";
        private String lte = "2018/04/17 15:55:22 +0800";

        public String getGte() {
            return gte;
        }

        public void setGte(String gte) {
            this.gte = gte;
        }

        public String getLte() {
            return lte;
        }

        public void setLte(String lte) {
            this.lte = lte;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("gte", gte)
                    .add("lte", lte)
                    .toString();
        }
    }

    public static class Sort {
        @JSONField(name = "es_timestamp")
        private String esTimestamp = "desc";

        public String getEsTimestamp() {
            return esTimestamp;
        }

        public void setEsTimestamp(String esTimestamp) {
            this.esTimestamp = esTimestamp;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("esTimestamp", esTimestamp)
                    .toString();
        }
    }
}