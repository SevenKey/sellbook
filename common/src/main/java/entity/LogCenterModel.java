package entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.MoreObjects;

import java.util.List;

/**
 * @author weijianyu
 */
public class LogCenterModel {
    @JSONField(name = "rows")
    private List<RowItem> rows;
    private String from;
    private String interval;
    private String total;
    private String size;

    public List<RowItem> getRows() {
        return rows;
    }

    public void setRows(List<RowItem> rows) {
        this.rows = rows;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("rows", rows)
                .add("from", from)
                .add("interval", interval)
                .add("total", total)
                .add("size", size)
                .toString();
    }
}
