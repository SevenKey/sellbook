package httpcode;

/**
 * HttpStatusCodeEnum  http返回值code枚举类
 * Created by weijianyu on 2016/12/16.
 */
public enum HttpStatusCodeEnum {
    SUCCESS(200, "成功"),
    ERROR(500, "失败");

    private int code;

    private String value;

    private HttpStatusCodeEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
