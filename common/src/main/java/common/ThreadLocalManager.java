package common;

import entity.User;

/**
 * 线程变量管理
 * Created by weijianyu on 2017/4/24.
 */
public class ThreadLocalManager {
    private static ThreadLocal<User> userSession = new ThreadLocal<User>();

    private ThreadLocalManager() {

    }

    /**
     * 获取user信息
     *
     * @return 当前user信息
     */
    public static User getUserSession() {
        if (userSession.get() == null) {
            userSession.set(new User());
        }
        return userSession.get();
    }

    /**
     * 存放user信息
     *
     * @param user 当前user信息
     */
    public static void setUserSession(User user) {
        userSession.set(user);
    }

    /**
     * clean
     */
    public static void clear() {
        userSession.remove();
    }
}
