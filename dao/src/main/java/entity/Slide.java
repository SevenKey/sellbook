package entity;

public class Slide {
    private Integer id;

    private String pictureUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl == null ? null : pictureUrl.trim();
    }

    @Override
    public String toString() {
        return "Slide{" +
                "id=" + id +
                ", pictureUrl='" + pictureUrl + '\'' +
                '}';
    }
}