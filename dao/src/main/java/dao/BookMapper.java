package dao;

import entity.Book;
import entity.BookExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Book record);

    int insertSelective(Book record);

    List<Book> selectByExample(BookExample example);

    Book selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Book record);

    int updateByPrimaryKey(Book record);

    /**
     * 自建模糊查询
     *
     * @param bookName 书名
     * @return 符合条件的所有书籍
     */
    List<Book> selectByBookName(@Param("bookName") String bookName);
}