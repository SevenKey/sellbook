-- auto-generated definition
create table books
(
	id int not null auto_increment
		primary key,
	book_name varchar(255) default '' not null,
	author varchar(255) default '' not null,
	press varchar(255) default '' not null,
	press_time varchar(255) default '' not null,
	price int not null,
	picture_url varchar(255) default '' not null,
	type smallint(2) unsigned not null,
	parent smallint(2) unsigned not null,
	detail_name varchar(255) default '' not null,
	descript varchar(255) default '' not null,
	translator varchar(255) default '' not null
)
;

comment on column books.id is '主键id'
;

comment on column books.book_name is '书名'
;

comment on column books.author is '作者'
;

comment on column books.press is '出版社'
;

comment on column books.press_time is '出版时间'
;

comment on column books.price is '价格单价(分)'
;

comment on column books.picture_url is '图片url'
;

comment on column books.type is '类型 1为新品 2为普通商品'
;

comment on column books.parent is '父类'
;

comment on column books.detail_name is '详细书名'
;

comment on column books.descript is '书籍简介'
;

comment on column books.translator is '翻译者'
;

