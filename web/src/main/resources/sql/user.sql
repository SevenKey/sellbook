-- auto-generated definition
create table user
(
	id int not null auto_increment
		primary key,
	user_name varchar(255) default '' not null,
	email varchar(255) default '' not null,
	password varchar(32) default '' not null,
	phone varchar(11) default '' not null
)
;

comment on column user.id is '主键id'
;

comment on column user.user_name is '用户名'
;

comment on column user.email is '邮箱'
;

comment on column user.password is '密码'
;

comment on column user.phone is '电话'
;

