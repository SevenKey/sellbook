-- auto-generated definition
create table slide
(
	id int not null auto_increment
		primary key,
	picture_url varchar(255) null,
	constraint slide_id_uindex
		unique (id)
)
;

comment on table slide is '轮播图'
;

comment on column slide.id is 'id'
;

comment on column slide.picture_url is '图片路径'
;

