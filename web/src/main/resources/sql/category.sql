-- auto-generated definition
create table category
(
	id int not null auto_increment
		primary key,
	category_name varchar(255) default '' not null,
	level smallint(2) default '-1' not null,
	parent smallint(2) not null
)
;

comment on column category.id is '主键id'
;

comment on column category.category_name is '类目名称'
;

comment on column category.level is '类目级别'
;

comment on column category.parent is '类目父级'
;

