package exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 全局exception处理类
 * Created by weijianyu on 2016/12/14.
 */
@ControllerAdvice
public class AppWideExceptionHandler {
    @ExceptionHandler(TestException.class)
    public String DealTestException() {
        return "error/test";
    }
}
