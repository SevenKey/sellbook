package exception;

/**
 * Created by weijianyu on 2016/12/14.
 */
public class TestException extends Exception {

    private static final long serialVersionUID = -2992284109668400826L;

    public TestException(String message) {
        super(message);
    }
}
