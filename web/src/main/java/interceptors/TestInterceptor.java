package interceptors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.net.URI;

/**
 * 登录拦截器
 * Created by weijianyu on 2017/4/10.
 */
public class TestInterceptor implements HandlerInterceptor {

    private static final String HTTPS = "http";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        URI requestURI = new URI(request.getRequestURL().toString());

        // 协议过滤
        String protocol = requestURI.getScheme();
        if (!StringUtils.equals(protocol, HTTPS)) {
            PrintWriter writer = response.getWriter();
            writer.write("{'code':'99','message':'error'}");
            return false;
        }

        // 开关过滤
        String requestPath = requestURI.getPath();
        System.out.println(requestPath);
        return false;
    }


    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
