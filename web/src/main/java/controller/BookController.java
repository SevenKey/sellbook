package controller;

import entity.Book;
import httpcode.HttpStatusCodeEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import params.QueryParam;
import service.BookService;
import vo.ResultVO;

import javax.validation.Valid;
import java.util.List;

/**
 * 书籍信息
 * Created by weijianyu on 2017/4/24.
 */
@Controller
@ResponseBody
@RequestMapping("/book")
public class BookController {
    private static final Logger logger = LogManager.getLogger(BookController.class);

    private final BookService bookService;

    /**
     * 构造方法
     * 初始化
     *
     * @param bookService 书籍服务
     */
    @Autowired
    public BookController(BookService bookService) {
        Assert.notNull(bookService, "bookService must not null");
        this.bookService = bookService;
    }

    /**
     * 新书推荐
     *
     * @return 书籍列表
     */
    @GetMapping(value = "newbooks")
    public ResultVO<List<Book>> newBooks() {
        logger.info("书籍服务 新书推荐");
        return bookService.newBooksList();
    }

    /**
     * 通过id查询书籍详情
     *
     * @param id 书籍id
     * @return 该id的书籍详情信息
     */
    @GetMapping(value = "detail")
    public ResultVO<Book> detail(@RequestParam("id") int id) {
        logger.info("书籍服务 通过id{}查询详情", id);
        return bookService.bookDetail(id);
    }

    /**
     * 排行榜书籍
     *
     * @return 排行
     */
    @GetMapping(value = "rankbooks")
    public ResultVO<List<Book>> rankBooks() {
        logger.info("书籍服务 排行榜");
        return bookService.rankBooks();
    }

    /**
     * 书籍搜索
     *
     * @param query 查询参数
     * @return 查询到的书籍
     */
    @PostMapping(value = "search")
    public ResultVO<List<Book>> search(@RequestBody QueryParam query) {
        logger.info("书籍服务 搜索 {}", query.getBookName());
        return bookService.search(query.getBookName());
    }

    @GetMapping(value = "classify")
    public ResultVO<List<Book>> classify(@RequestParam("id") int id) {
        return bookService.classify(id);
    }
}


