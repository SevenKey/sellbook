package controller;

import entity.Category;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import service.CategoryService;
import vo.ResultVO;

import java.util.List;

/**
 * 类目控制类
 * Created by weijianyu on 2017/4/25.
 */
@Controller
@ResponseBody
@RequestMapping("/category")
public class CategoryController {

    private static final Logger logger = LogManager.getLogger(CategoryController.class);

    private final CategoryService categoryService;

    /**
     * 构造方法
     * 初始化
     *
     * @param categoryService 类目服务
     */
    @Autowired
    public CategoryController(CategoryService categoryService) {
        Assert.notNull(categoryService, "categoryService must not null");
        this.categoryService = categoryService;
    }

    /**
     * 获取目录列表
     *
     * @return 目录列表
     */
    @GetMapping(value = "list")
    public ResultVO<List<Category>> categoryList() {
        logger.info("获取目录列表");
        return categoryService.categoryList();
    }

    /**
     * 获取分类
     *
     * @return 返回分类信息
     */
    @GetMapping(value = "parents")
    public ResultVO<List<Category>> parentCategorys() {
        logger.info("获取分类");
        return categoryService.parentCategorys();
    }

}
