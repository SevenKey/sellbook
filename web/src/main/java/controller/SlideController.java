package controller;

import entity.Slide;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import service.SlideService;
import vo.ResultVO;

import java.util.List;

/**
 * 轮播图
 * Created by weijianyu on 2017/5/9.
 */
@Controller
@ResponseBody
@RequestMapping("/slide")
public class SlideController {

    private static final Logger logger = LogManager.getLogger(SlideController.class);

    private final SlideService slideService;

    /**
     * 构造函数
     * 初始化
     *
     * @param slideService 轮播图服务
     */
    @Autowired
    public SlideController(SlideService slideService) {
        Assert.notNull(slideService, "slideService must not null!");
        this.slideService = slideService;
    }

    /**
     * 获取轮播图
     *
     * @return 轮播图信息
     */
    @GetMapping(value = "list")
    public ResultVO<List<Slide>> list() {
        logger.info("获取轮播图");
        return slideService.list();
    }
}
