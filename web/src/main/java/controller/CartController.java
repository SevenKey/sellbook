package controller;

import com.alibaba.fastjson.JSONObject;
import entity.Cart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import service.CartService;
import vo.ResultVO;


/**
 * 购物车控制层
 * Created by weijianyu on 2017/5/19.
 */
@Controller
@ResponseBody
@RequestMapping("/cart")
public class CartController {
    private static final Logger logger = LogManager.getLogger(CartController.class);

    private final CartService cartService;

    /**
     * 构造方法
     * 初始化
     *
     * @param cartService 购物车服务
     */
    @Autowired
    public CartController(CartService cartService) {
        Assert.notNull(cartService, "cartService must not null!");
        this.cartService = cartService;
    }

    /**
     * 添加到购物车
     *
     * @param cart 购物信息
     * @return 返回信息
     */
    @PostMapping(value = "add")
    public ResultVO<JSONObject> add(@RequestBody Cart cart) {
        logger.info("购物车 添加到购物车 {}", cart);
        return cartService.add(cart);
    }

    /**
     * 获取购物车列表
     *
     * @param cart 用户信息
     * @return 购物车列表
     */
    @PostMapping(value = "list")
    public ResultVO<JSONObject> list(@RequestBody Cart cart) {
        logger.info("购物车 查看购物车 {}", cart);
        return cartService.list(cart);
    }

    /**
     * 删除商品
     *
     * @param cart 要删除的商品
     * @return 信息
     */
    @PostMapping(value = "delete")
    public ResultVO<String> delete(@RequestBody Cart cart) {
        logger.info("购物车 删除商品 {}", cart);
        return cartService.delete(cart);
    }
}
