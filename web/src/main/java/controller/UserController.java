package controller;

import com.google.gson.JsonObject;
import entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import service.UserService;
import vo.ResultVO;

import java.util.Calendar;

/**
 * 用户操作控制器
 * Created by weijianyu on 2017/3/11.
 */
@Controller
@ResponseBody
@RequestMapping(value = "/user")
public class UserController {

    private final Logger logger = LogManager.getLogger(UserController.class);
    private final UserService userService;

    /**
     * 构造函数
     * 初始化
     *
     * @param userService 用户服务
     */
    @Autowired
    public UserController(UserService userService) {
        Assert.notNull(userService, "userService must not null");
        this.userService = userService;
    }


    /**
     * 注册方法
     *
     * @param register 注册的用户信息
     * @return 注册信息
     */
    @PostMapping(value = "register")
    public ResultVO<User> registed(@RequestBody User register) {

        logger.info("注册 register:{}", register.toString());
        return userService.registed(register);
    }

    /**
     * 登录服务
     *
     * @param user 登录的用户信息
     * @return 登录信息
     */
    @PostMapping(value = "login")
    public ResultVO<JsonObject> login(@RequestBody User user) {

        logger.info("登录 register:{}", user.toString());
        return userService.login(user);
    }

    /**
     * 通过cookie获取登录人信息
     *
     * @return 用户信息
     */
    @PostMapping(value = "loginer")
    public ResultVO<User> loginer() {
        logger.info("根据cookie获取登录信息");
        return userService.getLoginer();
    }

    @GetMapping("test")
    public ResultVO<User> test11(){
        ResultVO<User> response = new ResultVO<>();
        response.setCode(0);
        response.setData(new User());
        response.setErrorMessage("");

        userService.test1();
        return response;
    }

    public static void main(String[] args) {

    }
}
